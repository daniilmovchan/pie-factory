from pulp import LpMaximize, LpProblem, LpStatus, lpSum, LpVariable


def calculate_simplex_pie(data):
    model = LpProblem(name="max_profit", sense=LpMaximize)
    x = []

    x.append(LpVariable(name="x1", lowBound=0))
    x.append(LpVariable(name="x2", lowBound=0))
    x.append(LpVariable(name="x3", lowBound=0))
    x.append(LpVariable(name="x4", lowBound=0))

    bound_data = bounds(data)
    for bound in bound_data:
        if bound[1] == 'upBound':
            x[int(bound[0])].upBound = int(bound[2])
        else:
            if bound[1] == 'lowBound':
                x[int(bound[0])].lowBound = int(bound[2])

    model += (int(data['flour-x1']) * x[0]
              + int(data['flour-x2']) * x[1]
              + int(data['flour-x3']) * x[2]
              + int(data['flour-x4']) * x[3]
              <= int(data['max-flour']), "затраты муки")

    model += (int(data['milk-x1']) * x[0]
              + int(data['milk-x2']) * x[1]
              + int(data['milk-x3']) * x[2]
              + int(data['milk-x4']) * x[3]
              <= int(data['max-milk']), "затраты молока")

    model += (int(data['egg-x1']) * x[0]
              + int(data['egg-x2']) * x[1]
              + int(data['egg-x3']) * x[2]
              + int(data['egg-x4']) * x[3]
              <= int(data['max-egg']), "затраты яиц")

    obj_func = int(data['price-x1']) * x[0] \
               + int(data['price-x2']) * x[1] \
               + int(data['price-x3']) * x[2] \
               + int(data['price-x4']) * x[3]
    model += obj_func
    status = model.solve()
    output = []
    if status == 1:
        output.append(model.objective.value())
        output.append([])
        for var in model.variables():
            output[1].append(var.value())
    return output


def bounds(data):
    bond_data = []
    bond_data.append([data['constraint-1'], data['constraint-1-mos'], data['constraint-1-value']])
    bond_data.append([data['constraint-2'], data['constraint-2-mos'], data['constraint-2-value']])
    bond_data.append([data['constraint-3'], data['constraint-3-mos'], data['constraint-3-value']])
    return bond_data