from django.http import HttpResponse
from django.shortcuts import render

from pie.services.simplex_pie import calculate_simplex_pie


def index(request):
    return render(request, 'pie/index.html')


def calculate(request):
    if request.method == 'POST':
        data = request.POST
        result = calculate_simplex_pie(data)
        if result.__len__() == 2:
            return render(request, 'pie/result.html', {'max_profit': result[0],
                                                       "result_x1": result[1][0],
                                                       "result_x2": result[1][1],
                                                       "result_x3": result[1][2],
                                                       "result_x4": result[1][3],
                                                       })
        return HttpResponse("Error!")  # methods must return HttpResponse