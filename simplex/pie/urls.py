from django.urls import path

from pie.views import *

urlpatterns = [
    path('', index),
    path('calculate/', calculate),
]